import express, { RouterOptions, RequestHandler } from 'express';
import { MainInstance } from './index';
import Pug from "pug";
import { assert } from 'console';

/**
 * This class is the blueprint for an Endpoint. Extend from it and set 
 *
 * @export
 * @class Endpoint
 */
export abstract class Endpoint {
    /**
     * Elterninstanz der Main Methode, welche alle benötigten Objekte verwaltet
     * @type {MainInstance}
     * @memberof Endpoint
     */
    parent: MainInstance;

    /**
     * Legt fest in welchem Namespace das Modul läuft
     * @type {string}
     * @memberof Endpoint
     */
    routeString: string;

    /**
     * Beschreibt, welche Funktionen auf welche Route lauschen
     * @see {Route}
     * @type {Array<Route>}
     * @memberof Endpoint
     */
    routes:Array<Route> = new Array<Route>();

    /**
     * Legt fest ob die Route genutzt werden soll
     * @static
     * @type {boolean}
     * @memberof Endpoint
     */
    static isEnabled:boolean = true;

    expressRouter?:express.Router;

    /**
     * Leitet Zugriffe auf die Express-Instanz auf die Main-Instanz um
     * @readonly
     * @memberof Endpoint
     */
    get app() {
        // return this.parent.app
        return this.expressRouter;
    }

    /**
     * Leitet Zugriffe auf die MySQL-Instanz auf die Main-Instanz um
     * @readonly
     * @memberof Endpoint
     */
    get sql() { return this.parent.sql }


    /**
     * Bietet eine Interface, um auf die Datenbank zuzugreifen
     * @readonly
     * @memberof Endpoint
     */
    get query() { return this.parent.sql.q}

    static className() {
        return this.name;
    }

    /**
     * Erstellt eine Endpoint-Instanz
     * @param {MainInstance} parent
     * @param {string} [route]
     * @memberof Endpoint
     */
    constructor(parent:MainInstance, route?:string) {
        assert(parent, "Endpoint needs a reference to the MainInstance (which in turn contains express)");
        this.parent = parent;
        this.routeString = route ?? "";
        this.expressRouter = express.Router();
    }

    public setupExpressRouter() {
        for(let r in this.routes) {
            this.setupRouter(this.routes[r]);
        }
    }

    /**
     * Hängt die angegebene Route zu Express an
     * @param {Route} r
     * @param {Endpoint} reference
     * @memberof MainInstance
     */
    public setupRouter(r: Route) {
        if (!this.isRouteInUse(this.routeString + r.route)) {
            switch (r.method.toUpperCase()) {
                case "GET":
                    this.app?.get(r.route, r.routeArray.map(e => e.bind(this)));
                    break;

                case "POST":
                    this.app?.post(r.route, r.routeArray.map(e => e.bind(this)));
                    break;

                case "PUT":
                    this.app?.put(r.route, r.routeArray.map(e => e.bind(this)));
                    break;

                case "DELETE":
                    this.app?.delete(r.route, r.routeArray.map(e => e.bind(this)));
                    break;

                case "ALL":
                    this.app?.all(r.route, r.routeArray.map(e => e.bind(this)));
                    break;

                default:
                    throw new Error("Unimplemented or not supported method " + r.method.toUpperCase());
            }
        } else {
            console.log(this.routeString + r.route + " is in use");
        }
    }
/* 
    private isRouteInUse(route: string) {
        let stack = this.app?._router.stack;
        for (let r in stack) {
            if (stack[r].route?.path == route) {
                console.log(stack);
                // return true;
                return false;   //TODO create code that differentiates between routes and methods
            }
        }
        return false;
    } */

    private isRouteInUse(route: string) {
        return false;
    }

    /* Login Functions */
    checkSession(req: express.Request,
        res: express.Response,
        next: express.NextFunction) {
            if ((<any>req.session))
            if ((<any>req.session).loggedIn ?? false) {
                next();
            } else {
                res.sendStatus(403);
            }
        }
}

export interface RouteArray {
    [i:number]:Route;
}

/**
 * Stellt eine Route dar
 *
 * @export
 * @interface Route
 */
export interface Route {

    /**
     * Beschreibt welche Methode die Route verwendet.
     * Zulässige Methoden:
     * - GET
     * - POST
     * - PUT
     * - DELETE
     * - ALL (Äquivalent zu express.all - Antwortet bei allen Methoden)
     * @type {string}
     * @memberof Route
     */
    method:string;

    /**
     * Gibt an unter welchen Pfad die Route erreichbar ist (http://<url>/<route>)
     * @type {string}
     * @memberof Route
     */
    route:string;

    /**
     * Nutze stattdessen Route.routeArray
     * @deprecated
     * @type {boolean}
     * @memberof Route
     */
    login?: boolean;

    /**
     * Nutze stattdessen Route.routeArray
     * @deprecated
     * @memberof Route
     */
    callback?:(req: express.Request, res: express.Response, next?:express.NextFunction) => void ;

    /**
     * Nutze stattdessen Route.routeArray
     * @deprecated
     * @memberof Route
     */
    loginCallback?:(req: express.Request, res: express.Response, next:express.NextFunction) => void;

    /**
     * Beschreibt, welche Routen der Request nimmt sobald er aufgerufen wurde.
     * Wird von links nach rechts ausgeführt
     *
     * @type {Array<RequestHandler<any, any, any, any>>}
     * @memberof Route
     */
    routeArray: Array<RequestHandler<any, any, any, any>>;
}