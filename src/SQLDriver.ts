import { createConnection, Connection } from 'mysql';
import { MainInstance } from '.';
import queryStrings from './queries.json';

export class SQLDriver {

    databasename: string;
    q: any = queryStrings;

    private conn_!: Connection | null;
    /**
     * Gibt ein Verbindungsobjekt zurück
     *
     * @readonly
     * @memberof SQLDriver
     */
    get connection() {
        if (this.conn_ == null) {
            this.conn_ = createConnection({
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: this.databasename
            });
        }

        this.conn_.config.queryFormat = (query, values) => {
            if (!values) return query;
            return query.replace(/\:(\w+)/g, (txt: any, key: any) => {
                if (values.hasOwnProperty(key)) {
                    return this.conn_!.escape(values[key]);
                }
                return txt;
            });
        }

        return this.conn_;
    }

    /**
     * Creates an instance of SQLDriver.
     * @param {string} dbname Der Name der datenbank
     * @memberof SQLDriver
     */
    constructor(dbname: string) {
        this.databasename = dbname;
    }

    /**
     * Führt eine SQL Query in der Datenbank aus
     *
     * @template newT
     * @param {string} qry
     * @param {(object | null)} [params]
     * @return {*}  {Promise<SQLResponse>}
     * @memberof SQLDriver
     */
    query<newT>(qry: string, params?: object | null): Promise<SQLResponse> {
        return new Promise((res, rej) => {
            this.connection.query(
                qry,
                params,
                (error, results) => {
                    if (error) throw error; //res(new SQLException(error));
                    try {
                        if (results.length) {
                            let responseArray = new Array<newT>();
                            for (let g in results) {
                                responseArray.push(<newT>results[g]);
                            }

                            res(new SQLResponse(responseArray));
                        } else {
                            //TODO implement something that fires when nothing is in the selected table
                            //or when the response didnt contain any data
                            res(new SQLResponse([]));
                        }
                    } catch (e) {
                        res(new SQLResponse(e));
                    }
                });
        });
    }
}

/**
 * @deprecated
 * @export
 * @interface ExamplePhoto
 */
export interface ExamplePhoto {
    id: number;
    name: string;
    description: string;
    filename: string;
    isPublished: boolean;
    views: number;
}


export class RestResponse {
    /**
     * Inhalt der Exception
     *
     * @type {*}
     * @memberof SQLResponse
     */
    content: any = null;

    constructor(data?:any) {
        this.content = data;
    }

    /**
     * Gibt das Objekt als JSON-String zurück
     *
     * @return {*} 
     * @memberof SQLResponse
     */
    toClient() {
        return JSON.stringify(this);
    }
}

/**
 * Dieses Objekt wird bei einer erfolgreichen SQL-Ausführung zurückgegeben
 *
 * @export
 * @class SQLResponse
 */
export class SQLResponse extends RestResponse {
    /**
     * Stellt den Status der Anfrage dar
     * @type {string}
     * @memberof SQLResponse
     */
    status: string = "~Update";

    /**
     * Creates an instance of SQLResponse.
     * @param {*} [data]
     * @memberof SQLResponse
     */
    constructor(data?: any) {
        super(data);
    }
}

/**
 * Diese Klasse stellt einen Userspace-Fehler dar welcher im Code selbst passiert
 * @export
 * @class SQLError
 * @extends {SQLResponse}
 */
export class SQLError extends SQLResponse {
    status: string = "~Error";
    constructor(data?: any) {
        super(data);
    }
}

/**
 * Diese Klasse stellt einen SQL Fehler dar
 *
 * @export
 * @class SQLException
 * @extends {SQLError}
 */
export class SQLException extends SQLError {
    constructor(data?: any) {
        super(data);
        this.content = {
            "code": data.code
        }
    }
}