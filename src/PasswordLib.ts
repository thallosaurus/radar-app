import bcrypt from 'bcrypt';

const saltRounds = 10;

namespace Password {
    export function hash(password: string, callback: any) {
        bcrypt.genSalt(saltRounds, function (err, salt) {
            if (err) {
                throw err
            } else {
                bcrypt.hash(password, salt, function (err, hash) {
                    if (err) {
                        throw err
                    } else {
                        //   console.log(hash)
                        callback(hash);
                    }
                })
            }
        })
    }

    export function verify(enteredPassword: any, dbPassword: string) {
        return bcrypt.compareSync(enteredPassword, dbPassword);
    }

    export function hasPermission(perm:number, key:number) {
        return (perm & key) > 0;
    }
}

export default Password;