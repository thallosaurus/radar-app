import { MainInstance } from "..";
import { Endpoint } from "../Endpoint";
import express from 'express';
import session from "express-session";
import Password from '../PasswordLib';
import { SQLResponse } from "../SQLDriver";
import config from '../config.json';

export default class LoginEndpoint extends Endpoint {

    static isEnabled: boolean = true;

    constructor(parent: MainInstance) {
        super(parent, "/");

        // this.expressRouter?.use(this.)

        this.app?.use((req, res, next) => {
            if (req.method === 'POST') {
                const formData: any = {}
                req.on('data', data => {

                    // Decode and parse data 
                    const parsedData =
                        decodeURIComponent(data).split('&')

                    for (let data of parsedData) {

                        let decodedData = decodeURIComponent(
                            data.replace(/\+/g, '%20'))

                        const [key, value] =
                            decodedData.split('=')

                        // Accumulate submitted  
                        // data in an object 
                        formData[key] = value;
                    }

                    // Attach form data in request object 
                    req.body = formData
                    next()
                })
            } else {
                next()
            }
        })

        /*this.app?.use((req: express.Request,
            res: express.Response,
            next: express.NextFunction) => {
            // console.log(req.session);
            next();
        });*/
        // this.expressRouter?.use(this.checkSession);

        this.routes = [
            {
                method: "POST",
                route: "/login",
                routeArray: [this.loginUser]
            },
            {
                method: "GET",
                route: "/logout",
                routeArray: [this.logoutUser]
            }
        ];
    }

    async loginUser(req: express.Request,
        res: express.Response,
        next: express.NextFunction) {
        /*if (!(<any>req.session).loggedIn) {
            (<any>req.session).loggedIn = false;
        }*/
        // console.log(req);

        let b = await this.isUserKnown(req.body.username, req.body.password);

        if (b) {
            let infos = await this.getDetails(req);

            //TODO Auto Logout?
            /*if (config.enableAutoLogoff) {
                setTimeout(() => {
                    (<any>req.session).loggedIn = false;
                })
            }*/

            Object.assign((<any>req.session), infos.content[0]);
            (<any>req.session).loggedIn = true;
            (<any>req.session).isRootSession = this.isRootUser(req);
            console.log(req.session);

            if (req.body.referer) {
                res.redirect(req.body.referer);
            } else {
                res.send((<any>req.session).loggedIn);
            }
        } else {
            res.redirect("/" + (MainInstance.isDev() ? "dev/" : "")); /* + "app");*/
        }
    }

    isRootUser(req: express.Request) {
        return req.body.username == "root" && req.body.password == config.rootPassword && (<any>req.session).perm === Number.MAX_SAFE_INTEGER;
    }

    logoutUser(req: express.Request,
        res: express.Response,
        next: express.NextFunction) {
        if (!(<any>req.session).loggedIn) {
            (<any>req.session).loggedIn = false;
        }
        (<any>req.session).loggedIn = false;
        res.redirect("/" + (MainInstance.isDev() ? "dev/" : "")); /* + "app");*/
        // res.send((<any>req.session).loggedIn);
    }

    async isUserKnown(username: string, password: string) {
        if (username == "root" && password == config.rootPassword) {
            return true;
        }

        let q = await this.sql.query(this.query.getUserCount, {
            uname: username
        });

        if (q.content.length > 0) {
            return Password.verify(password, q.content[0]["hash"]);
        }
        return false;
    }

    async getDetails(req: express.Request) {
        if (req.body.username != "root") {
            let v = await this.sql.query(this.query.getUserinfo, {
                uname: req.body.username
            });
            // console.log("Userdetails:", v);
            return v;
        } else {
            return new SQLResponse([{
                userid: 0,
                username: "root",
                perm: Number.MAX_SAFE_INTEGER
            }]);
        }
    }
}

module.exports = LoginEndpoint;