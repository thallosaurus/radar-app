import { MainInstance } from '..';
import {Endpoint, Route} from '../Endpoint';
import express from 'express';

export default class HelloWorldEndpoint extends Endpoint {
    static isEnabled: boolean = false;
    
    constructor(parent:MainInstance) {
        super(parent, "/helloworld");

        this.routes = [
            {
                method: "GET",
                route: "/",
                routeArray: [this.helloWorld]
            },
            {
                method: "GET",
                route: "/add",
                routeArray: [this.addColumn]
            },
            {
                method: "GET",
                route: "/premium",
                routeArray: [this.testNextFunction, this.neverShow]
            },
            {
                method: "GET",
                route: "/select",
                routeArray: [this.selectFromDatabase]
            },
            {
                method: "GET",
                route:"/login",
                routeArray: [this.loginUser]
            },
            {
                method: "GET",
                route: "/coffee",
                routeArray: [this.makeCoffee]
            }
        ]
    }

    public makeCoffee(req: express.Request, res: express.Response) {
        res.sendStatus(418);
    }

    public loginUser(req: express.Request, res: express.Response) {
        /*if (!req.session.isLoggedIn) {
            req.session.isLoggedIn = true;
        }*/
    }

    public logoutUser(req: express.Request, res: express.Response) {
        req.session.destroy(function (err) {
            
        })
    }

    public selectFromDatabase(req: express.Request, res: express.Response) {
        this.sql.query(this.query.selectQuery).then(
            (d) => {
                res.send(d.toClient());
            }
        )
    }

    public neverShow(req: express.Request, res: express.Response) {
        res.send("You'll never see this!");
    }

    private doExampleQuery(res: express.Response) {
        this.sql.query(this.query.testQuery).then(
            (d) => {
                res.send(d.toClient());
        });
    }

    private addShit(res: express.Response) {
        this.sql.query(this.query.addStuff).then(
            (d) => {
                res.send(d.toClient());
            }
        )
    }

    public testNextFunction(req: express.Request, res: express.Response, next: express.NextFunction) {
        res.sendStatus(403);
        // next('route');
    }

    public helloWorld(
        req: express.Request,
        res: express.Response) {
            this.doExampleQuery(res);
        }

    public addColumn(req: express.Request,
        res: express.Response) {
            this.addShit(res);
        }
}

module.exports = HelloWorldEndpoint;