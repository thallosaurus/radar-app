import { MainInstance } from "..";
import { Endpoint } from "../Endpoint";
import express from 'express';
import path from 'path';
import Pug from 'pug';
import session from "express-session";
import Password from "../PasswordLib";
import AppPermissions from '../AppPermissions';

const LoginVariables = {
    referer: (MainInstance.isDev() ? "/dev/" : "/"), /* + "app",*/
    login: (MainInstance.isDev() ? "/dev/" : "/") + "login",
    name: "Login"
}

// const route:string = (MainInstance.isDev() ? "/app" : "/");

export default class AppEndpoint extends Endpoint {

    static isEnabled: boolean = true;

    constructor(parent: MainInstance) {
        super(parent, "/");

        this.app?.use((req: express.Request,
            res: express.Response,
            next: express.NextFunction) => {
            next();
        });

        this.app?.use('/res', express.static('appClients/' + (process.env.APP_BUILD ?? "") + "/res"));

        this.routes = [
            {
                method: "GET",
                route: "/",
                routeArray: [this.getIndex]
            },
            {
                method: "GET",
                route: "/userenvs",
                routeArray: [this.checkSession, this.sendAppSettings]
            }
        ];
    }

    public getIndex(req: express.Request,
        res: express.Response) {
            req.params.file = req.params.file != undefined ? req.params.file : "index.html";
            if (req.params.file == "index.html") {
            if ((<any>req.session).loggedIn) {
                res.send(Pug.compileFile('appClients/' + ((process.env.APP_BUILD + "/") ?? "") + 'index.pug')());
            } else {
                res.send(Pug.compileFile('appClients/login.pug')(LoginVariables));
            }
        } else {
            res.sendFile(path.resolve('appClients/' + (process.env.APP_BUILD ?? "") + "/" + req.params.file));
        }
    }

    public getFile(req:express.Request,
        res: express.Response) {
            
        }

    public sendAppSettings(
        req: express.Request,
        res: express.Response) {
            res.send(JSON.stringify({
                dev_prefix: MainInstance.isDev(),
                permissions: {
                    showWipeButton: Password.hasPermission((<any>req.session).perm, AppPermissions.WIPE) && (<any>req.session).loggedIn,
                    showAddButton: Password.hasPermission((<any>req.session).perm, AppPermissions.ADD_COP) && (<any>req.session).loggedIn
                },
                engine: "osm"
            }));
        }
}

module.exports = AppEndpoint;
