import { MainInstance } from "..";
import { Endpoint } from "../Endpoint";
import express from 'express';
import Password from "../PasswordLib";
import AppPermissions from "../AppPermissions";
import config from '../config.json';
import { StaticFunctions } from '../StaticFunctions';

export default class CopsEndpoint extends Endpoint {
    static isEnabled: boolean = true;
    constructor(parent: MainInstance) {
        super(parent, "/cops");

        this.expressRouter?.use(this.checkSession);

        this.routes = [
            {
                method: "GET",
                route: "/get/:lat/:lon",
                routeArray: [this.getCopsInProximity]
            },
            {
                method: "GET",
                route: "/add/:type/:lat/:lon",
                routeArray: [this.canUserAdd, this.checkCooldownTime, this.addSighting]
            },
            {
                method: "GET",
                route: "/wipe",
                routeArray: [this.isServerInDebug, this.debugWipe]
            },
            {
                method: "GET",
                route: "/testProx/:lat/:lon",
                routeArray: [this.testProx]
            }
        ]
    }

    public checkCooldownTime(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction) {
        if (config.enableCooldownTime) {
            this.sql.query(this.query.getSightingLastFiveMinsById, {
                "user": (<any>req.session).userid,
                "minutes": config.cooldownTimeMinutes
            }).then(d => {
                // console.log(d);
                if (d.content[0].count < 1 || !(<any>req.session).isRootUser) {
                    next();
                } else {
                    res.send(403);
                    res.end();
                }
            });
        } else {
            next();
        }
    }

    public canUserAdd(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        let p = Password.hasPermission((<any>req.session).perm, AppPermissions.ADD_COP);
        // console.log("Can user add?", p);
        if (Password.hasPermission((<any>req.session).perm, AppPermissions.ADD_COP)) {
            next();
        } else {
            res.sendStatus(403);
        }
    }

    public testProx(req: express.Request,
        res: express.Response) {
        this.sql.query(this.query.getCountInProx, {
            "u_lat": req.params.lat ?? 0,
            "u_lon": req.params.lon ?? 0,
            "u_rad": config.scannerRadius
        }).then(
            (d) => {
                res.send(d.toClient());
            }
        )
    }

    getCopsInProximity(
        req: express.Request,
        res: express.Response
    ) {
        this.sql.query(this.query.getInRadius, {
            "u_lat": req.params.lat ?? 0,
            "u_lon": req.params.lon ?? 0,
            "u_rad": config.scannerRadius
        }).then(
            (d) => {
                res.send(d.toClient());
            }
        )
    }

    addSighting(
        req: express.Request,
        res: express.Response
    ) {
        if (req.params.lat && req.params.lon) {
            this.sql.query(this.query.addCop, {
                "user": (<any>req.session).userid,
                "type": req.params.type,
                "lat": req.params.lat,
                "lon": req.params.lon
            }).then(
                (d) => {
                    if (config.enableCarGarbageCollector) {
                        this.sql.query(this.query.getLastInsertedId).then(
                            e => {
                                // console.log(e);
                                // let timeout = StaticFunctions.ms2minutes(config.carGarbageCollectionTimeMinutes);
                                setTimeout(() => {
                                    this.sql.query(this.query.deleteSighting, {
                                        id: e.content[0].id
                                    });
                                }, StaticFunctions.minutes2ms(config.carGarbageCollectionTimeMinutes));    //TODO switch this to something else than milliseconds
                            });
                    }

                    res.send(d.toClient());
                }
            );
        }
    }

    deleteSightingMiddleware(id: number) {

    }

    isServerInDebug(req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        if (MainInstance.isDebug()) {
            next();
        } else {
            res.sendStatus(403);
        }
    }

    debugWipe(req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        this.sql.query(this.query.wipe).then((d) => {
            res.send(d.toClient());
        });
    }

    // private 
}

module.exports = CopsEndpoint;