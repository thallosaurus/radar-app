import express from "express";
import "./StaticFunctions";
import * as dotenv from "dotenv";
import { SQLDriver, ExamplePhoto, SQLResponse } from './SQLDriver';
import fs from "fs";
import { Endpoint, Route } from "./Endpoint";
import session from 'express-session';
import cors from 'cors';

dotenv.config();

/**
 * Dies ist die Main-Klasse.
 *
 * @export
 * @class MainInstance
 */
export class MainInstance {

    get port() : number { return parseInt(process.env.APP_PORT!); }

    modules: Array<Endpoint> = new Array<Endpoint>();
    app?: express.Application;
    sql: SQLDriver;

    /**
     * Creates an instance of MainInstance.
     * Erstellt eine Instanz von Express, stellt die Routen ein und lauscht auf den Port 3000 (standartmäßig)
     * @memberof MainInstance
     */
    constructor() {
        this.app = express();
        // this.setupApp();
        this.setupRoutes();
        this.listen(this.port);

        this.sql = new SQLDriver("test");
    }

    private activateExpressRouter(ref:Endpoint) {
        this.app?.use(ref.routeString, ref.expressRouter!);
    }

    private listen(p: number) {
        this.app!.listen(p, () => console.log(`Listening on http://localhost:${p}`));
    }

    private async setupRoutes() {
        this.setupGlobalMiddleware();
        //Search for modules in ./app/routes and enable them
        let dir = fs.readdirSync(process.cwd() + "/app/routes");
        for (let i = 0; i < dir.length; i++) {
            let name = dir[i].split(".");

            if (name[name.length - 1] === "js") {
                let m: any = await require(process.cwd() + "/app/routes/" + dir[i]);
                if (m.isEnabled) {
                    let mod = new m(this);
                    console.log("Adding Routes for: " + m.className());
                    mod.setupExpressRouter();
                    this.activateExpressRouter(mod);
                }
            }
        }
    }

    private setupGlobalMiddleware() {
        this.app?.use(cors());
        this.app?.use(function (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.set('Cache-Control', 'no-store');
            next();
        });

        this.app?.use(session({
            secret: 'test me',
            resave: false,
            saveUninitialized: true
        }));

        this.app?.use(function(req: express.Request,
            res: express.Response,
            next: express.NextFunction) { 
                try {
                    next();
                } catch (e:any) {
                    console.log(e);
                    res.send(e.toClient());
                }
            })
    }

    public static isDev() {
        return process.env.NODE_ENV == "dev" || false;
    }

    public static isDebug() {
        return process.env.NODE_DEBUG == "true" || false;
    }

    /**
     * Erstellt die Route zum App-Frontend
     * @deprecated
     * @private
     * @memberof MainInstance
     */
    private setupApp() {
        this.app?.use('/app', express.static('appClients/' + (process.env.APP_BUILD ?? "")));
    }
}

class EndpointInitError extends Error {
    constructor(ep: Endpoint) {
        super("Fehler beim Initialisieren des Moduls " + ep.constructor.name);
    }
}

const mainClass: MainInstance = new MainInstance();