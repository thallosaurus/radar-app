namespace AppPermissions {
    export const WIPE = 0b100;
    export const ADD_COP = 0b01;
}

export default AppPermissions;