export class StaticFunctions {
    static isDev() {
        return process.env.NODE_ENV == "dev" || false;
    }

    static seconds2ms(s:number) {
        return s * 1000; 
    }

    static minutes2ms(m:number) {
        return m * 1000 * 60;
    }

    static hours2ms(h:number) {
        return h * 1000 * 60 * 24;
    }
}