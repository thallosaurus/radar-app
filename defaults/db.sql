SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

CREATE TABLE `photo` (
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `description` text NOT NULL,
    `filename` varchar(255) NOT NULL,
    `isPublished` tinyint(4) NOT NULL,
    `views` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `photo`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `photo`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

COMMIT;