# radar-app

Usage:
First, clone this repo and run `npm install`

To start the app, run `node .`

To generate documents, run `npm run generate-docs`

To run tests, run `npm run test`

To run it, you have to have pm2 installed by running `npm install pm2 -g`