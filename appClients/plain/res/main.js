// REFACTOR Nr. 1

function isDev() {
    return globalThis.envs.dev_prefix ?? false;
}

function getParam(param) {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}

async function fetchConfig() {
    try {
        // console.log(urlPrefix);

        let response = await fetch(location.href + "userenvs", {
            credentials: "same-origin"
        });
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(response.statusText);
        }
    } catch (e) {
        alert(e.message);
    }
}

async function get(url, params) {
    return request(url,
        params,
        "GET");
    //let v = await validateData(r);
}

function validateData(data) {
    return new Promise((res, rej) => {
        switch (data.status) {
            case "~Update":
                res(data.content);
                break;

            case "~Error":
                rej(data);
                break;
        }
    })
}

async function request(url, params, method = "GET") {
    try {
        let urlPrefix = location.origin + ((isDev()) ? "/dev" : "");

        let response = await fetch(urlPrefix + url, {
            credentials: "same-origin"
        });
        if (response.ok) {
            return response.json();
        } else {
            throw new Error(response.statusText);
        }
    } catch (e) {
        alert(e.message);
    }
}

class WebApp {
    inited = false;
    intervalId = -1;

    constructor() {
        if (getParam("dbg")) {
            this.createDebugAdapter(getParam("dbg"));
        }

        this.lat = 0;
        this.lon = 0;
        this.map = new Map(this);
        this.initGeoApi();
    }

    startUpdateInterval() {
        setInterval(() => {
            this.getCops();
        }, 60000);
    }

    initGeoApi() {
        this.probeGeoAPI().then(() => {
            navigator.geolocation.watchPosition(this.locationWatcher.bind(this), (e) => {
                alert("Error: " + e.message);
            }, {
                enableHighAccuracy: true
            });
        });
    }

    async locationWatcher(pos) {
        this.lat = pos.coords.latitude;
        this.lon = pos.coords.longitude;
        if (this.intervalId == -1) {
            this.getCops();
            this.intervalId = setInterval(() => {
                this.getCops();
            }, 30000);
        }
        this.map.setOwnPosition(this.lat, this.lon);
    }

    probeGeoAPI() {
        return new Promise((res, rej) => {
            if ("geolocation" in navigator) res(); else rej();
        });
    }

    async addNewEntry() {
        await get("/cops/add/0/" + this.lat + "/" + this.lon);
        this.getCops();
    }

    async getCops() {
        let g = await get("/cops/get/" + this.lat + "/" + this.lon);
        if (g.content != null) {
            // debugger;
            let keys = Object.keys(this.map.copMarkers);
            g.content.forEach(e => {
                this.map.addPoliceCar(e);
                keys.splice(keys.indexOf("" + e.id), 1);  //Implicit Int -> String Conversion
            });

            // console.log(keys);

            this.map.cleanupMap(keys);
        }
    }

    async wipe() {
        let g = await get('/cops/wipe');
        location.reload();
    }

    createDebugAdapter(fnToken) {
        let dbg = document.createElement("script");
        if (fnToken == 1) {
            fnToken = prompt("Debug-Token:");
        }

        if (fnToken !== null) {
            dbg.dataset.consolejsChannel = fnToken;
            dbg.src = "https://remotejs.com/agent/agent.js";
            document.head.append(dbg);
            // showCSSGrid();
        }
    }

    getCoordsLatLng() {
        return [this.lat, this.lon];
    }
}

class Map {
    inited = false;

    constructor(parent) {
        this.parent = parent;
        this.copMarkers = {};
        this.changeMapCoordsOnUIpdate = true;

        this.mapObj = this.initMap();

        this.tileMap = this.createTileMap();
        this.tileMap.addTo(this.mapObj);
        L.control.scale().addTo(this.mapObj);

        this.markerClusterGroup = this.setupClusterGroup();
        this.markerClusterGroup.addTo(this.mapObj);

        this.currentPositionMarker = this.initMarker();
        this.currentPositionMarker.addTo(this.mapObj);

        if (globalThis.envs.permissions.showAddButton) {
            this.addButton = this.createMapSubmitButton();
            this.addButton.addTo(this.mapObj);
        }

        this.centerButton = this.createCenterButton();
        this.centerButton.addTo(this.mapObj);

        if (globalThis.envs.permissions.showWipeButton) {
            this.wipeButton = this.createDebugWipeButton();
            this.wipeButton.addTo(this.mapObj);
        }
    }

    setupClusterGroup() {
        return L.layerGroup();
    }

    initMap(mapid = 'mapid') {
        return L.map(mapid).setView([0, 0], 19);
    }

    createTileMap() {
        switch (globalThis.envs.engine) {
            case "osm":
                return L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 19,
                    attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
                });

            case "tangramm":
                return Tangram.leafletLayer({ scene: 'res/scene.yaml' });
            case "mapbox":
            default:
                return L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: 'mapbox/streets-v11',
                    tileSize: 512,
                    zoomOffset: -1,
                    accessToken: 'pk.eyJ1IjoidGhhbGxvc2F1cnVzIiwiYSI6ImNraXJjNnMzZzAzZHEyc3Bkc2Y0NThqeWcifQ.Y6irr37lcWxqIw1GR3ANUg'
                });
            }
        }

        initMarker() {
            return L.marker([0, 0]);
        }

        setOwnPosition(lat, lon) {
            // console.log(this.currentPositionMarker);
            this.currentPositionMarker.setLatLng([lat, lon]);
            if (!this.inited) {
                this.mapObj.setView([lat, lon]);
                this.inited = true;
            }
        }

        createMapSubmitButton() {
            return L.easyButton('fa fa-plus-circle', (btn, map) => {
                // alert("Hey there!");
                this.parent.addNewEntry();
            });
        }

        createCenterButton() {
            return L.easyButton('fa fa-user', (btn, map) => {
                // this.changeMapCoordsOnUIpdate = !this.changeMapCoordsOnUIpdate;
                this.mapObj.setView(this.parent.getCoordsLatLng());
            })
        }

        createDebugWipeButton() {
            return L.easyButton('fa fa-trash', (btn, map) => {
                this.parent.wipe();
            });
        }

        createNewPoliceMarker(car) {
            console.log(car);
            let carMarker = L.marker([car.db_lat, car.db_lon]);
            carMarker.setIcon(copCar);
            return carMarker;
        }

        createPoliceRadius(radius) {
            let circleMarker = L.circle([radius.db_lat, radius.db_lon], circleOptions);
            return circleMarker;
        }

        addPoliceCar(car) {
            if (this.isCarKnown(car)) {
                this.copMarkers[car.id].car.setLatLng([car.db_lat, car.db_lon]);
                this.copMarkers[car.id].circle.setLatLng([car.db_lat, car.db_lon]);
            } else {
                this.copMarkers[car.id] = {
                    car: null,
                    circle: null
                }
                this.copMarkers[car.id].car = this.createNewPoliceMarker(car);
                this.copMarkers[car.id].circle = this.createPoliceRadius(car);

                for (let k of Object.keys(this.copMarkers[car.id])) {
                    this.copMarkers[car.id][k].addTo(this.markerClusterGroup);
                }
            }
        }

        cleanupMap(keys) {
            for (let k of keys) {
                this.markerClusterGroup.remove(this.copMarkers[parseInt(k)].car);
                this.markerClusterGroup.remove(this.copMarkers[parseInt(k)].circle);

                delete this.copMarkers[k];
            }
        }

        setNewCar(car, marker, circle) {
            this.copMarkers[car.id].car = marker;
            this.copMarkers[car.id].circle = circle;
        }

        isCarKnown(car) {
            return Object.keys(this.copMarkers).indexOf("" + car.id) != -1;
        }
    }

    const copCar = L.icon({
        iconUrl: 'res/copcar.png',
        iconSize: [32, 32],
        iconAnchor: [16, 16],
        popupAnchor: [-3, -76],
    });

    const circleOptions = {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.25,
        radius: 50
    }

let webApp;

window.onload = async () => {
    globalThis.envs = await fetchConfig();
    Object.seal(globalThis.envs);
    console.log(globalThis.envs);

    webApp = new WebApp();
}
