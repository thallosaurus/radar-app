let lat, lon = null;

window.onload = () => {
    getLocationApi(getDataFromDB);
}

/* Geo API */
function getLocationApi(callback) {
    probeGeoAPI().then(() => {
        navigator.geolocation.watchPosition((pos) => { locationWatcher(pos) }, null, {
            enableHighAccuracy: true
        });
        callback();
    });
}

function locationWatcher(pos) {
    lat = pos.coords.latitude;
    lon = pos.coords.longitude;
    console.log(pos);
}

function probeGeoAPI() {
    return new Promise((res, rej) => {
        if ("geolocation" in navigator) res(); else rej();
    });
}

function fetchDBData(lat, lon) {
    return fetch(`../cops/get/${lat}/${lon}`);
}

function addDBData(lat, lon) {
    return fetch(`../cops/add/0/${lat}/${lon}`);
}

function alertDatabase() {
    if (lat != null && lon != null) {
        addDBData(lat, lon).then(
            res => {
                return res.json();
            }).then(res => {
                if (res.status != "~Update") {
                    alert("An error occured!");
                    console.log(res);
                }
                console.log(res);
                getDataFromDB();
            });
    }
}

function getDataFromDB() {
    fetchDBData(lat, lon).then(response => {
        return response.json();
    }).then(res => {
        appendToDebugLog(res.content);
    });
}

function appendToDebugLog(stuff) {
    let log = document.querySelector(".debugLog");
    log.innerHTML = "";

    for (let o in stuff) {
        let elem = document.createElement("li");
        elem.innerText = "Age: " + stuff[o].age + ", Lat: " + stuff[o].db_lat + ", Lon: " + stuff[o].db_lon + ", distance: " + stuff[o].distance + ", id: " + stuff[o].id;
        log.append(elem);
    }
}
